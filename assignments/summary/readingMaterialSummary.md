# **Summary**

# **IoT**


### **What is IoT?**

The Internet of Things is the network of physical objects or things embedded with electronics, software, sensors, and network connectivity, which enables these objects to collect and exchange data. 
`For Example`, have you ever imagined a smart refrigerator which can order the stock whenever it is empty or a car that slows down automatically whenever the road gets slippery? IoT enables you to do this and it has much more complicated applications in science and industry.


## **Industrial Revolutions**

- Industry 1.0 - Weave Loming,Mechanization,Steam Power

- Industry 2.0 - Mass production began with the help of a production line

- Industry 3.0 - This Revolution saw the evolution of electronics with automation etc.
- Industry 4.0 - The sensors and acutators were able to send data to the cloud using certain mechanisms.

![see the revolutions here](https://f0.pngfuel.com/png/986/893/unk-unk-unk-unk-unk-fourth-industrial-revolution-industry-4-internet-of-things-industry-4-png-clip-art.png)

## **Industry 3.0**

Data is stored in databases and represented in excels.

Let us dive into industry 3.0

**Field devices**

- sensors - A device which detects or measures a physical property and records, indicates, or otherwise responds to it.
- acutators - A device that causes a machine or other device to operate.

      
       

**Control Devices**
    
-  PCs
-  PLCs(Programmable Logic Controller)
-  CNCs(Computer Numerical Control)
- DCS(Distributed Control System)

**Stations** - Set of field devices and control devices form Stations.

**Work centers** - Set of Stations form work centers.(SCADA,HISTORIAN)

**Enterprise** - Set of work centers form Enterprise.(ERP,MES)

**Field Bus**

The bridge between the field devices and the control devices is called Field Bus.
![](/extras/pyramid.png)


**3.0 Protocols**

- MODBUS
- PROFINET
- CANopen
- EtherCAT


All these protocols are optimized for sending data to a central server inside the factory.


## **Industry 4.0**

Industry 4.0 is Industry 3.0 devices connected to the Internet (IoT).

The reasons why industry 4.0 is important are the benefits. It helps manufacturers with current challenges by becoming more flexible and reacting to changes in the market easier. It can increase the speed of innovation and is very consumer centered, leading to faster design processes.

**Various operations by Indutry 4.0**

- Dashboard
- Remote Web SCADA
- Remote control configuration of devices
- Predictive Maintenance
- Real time event stream processing
- Analytics with predictive models
- Automated device provisioning, Auto Discovery
- Real time alerts and alarms

**Arhitecture**
![](https://openiotfog.org/images/OpenIoTFog2Architecture.png)

**4.0 Protocols**

- MQTT
- AMQP
- CoAP
- OPC UA
- Websockets
- HTTP
- REST API


**Problems in 4.0 Upgrades**

 1. Cost
 2. Downtime
 3. Reliability

**Solution**

Get data from Industry 3.0 devices/meters/sensors without changes to the original device. And then send the data to the Cloud using Industry 4.0 devices.

**But how?**

Convert Industry 3.0 protocols to Industry 4.0 protocols. These conversions can be done using libraries available to get data from Industry 3.0 devices and further send it to Industry 4.0 devices.

- Identify most popular Industry 3.0 devices.
- Study protocols that these devices communicate.
- Get data from the Industry 3.0 device.
- Send the data to cloud for Industry 4.0 device.

## **Tools**

### **IoT TSDB Tools** - Store data in Time series databases.

- InfluxDB
- Kdb+
- Prometheus
- Graphite
- RRDTool
- TimescaleDB
- OpenTSDB
- Druid
- FaunaDB
- Thingsboard

### **IoT Platforms** - To Analyze data

- AWS IOT
- Google IoT
- Azure IoT
- Thingsboard

### **Alerts** 

Get Alerts based on your data using these platforms.

- Zaiper
- Twilio


## **Conclusion**

“If you think that the internet has changed your life, think again. The Internet of Things is about to change it all over again!” .

![](/extras/images.jpg)



